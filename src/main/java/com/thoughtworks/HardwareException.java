package com.thoughtworks;

public class HardwareException extends RuntimeException {
    public HardwareException(String msg) {
        super(msg);
    }
}
