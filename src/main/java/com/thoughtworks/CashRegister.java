package com.thoughtworks;

import java.util.Arrays;

public class CashRegister {

    private final Printer printer;

    public CashRegister() {
        this.printer = new Printer();
    }

    public CashRegister(Printer printer) {
        this.printer = printer;
    }

    public void process(Purchase purchase) {
        try {
            printer.print(purchase.toString());

        } catch (PrinterOutOfPaperException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            throw new HardwareException("Printer is out of paper.");
        }
    }
}