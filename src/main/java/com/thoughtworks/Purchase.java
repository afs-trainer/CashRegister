package com.thoughtworks;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class Purchase {
    private String content;

    public Purchase() {
        content = "content";
    }

    public String toString() {
        String time = timestamp();
        return this.content + time;
    }

    private static String timestamp() {
        return String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
    }
}