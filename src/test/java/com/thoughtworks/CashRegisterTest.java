package com.thoughtworks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CashRegisterTest {

    @Test
    void should_call_printer_when_process_given_purchase() {
        //given
        SpyPrinter spyPrinter = new SpyPrinter();
        CashRegister cashRegister = new CashRegister(spyPrinter);
        Purchase purchase = new Purchase();

        //when
        cashRegister.process(purchase);

        //then
        Assertions.assertEquals(1, spyPrinter.callCount);
    }

    @Test
    void should_call_printer_use_content_from_purchase_when_process_given_mock_purchase() {
        //given
        SpyPrinter spyPrinter = new SpyPrinter();
        CashRegister cashRegister = new CashRegister(spyPrinter);
        MockPurchase purchase = new MockPurchase();
        purchase.content = "given content";

        //when
        cashRegister.process(purchase);

        //then
        Assertions.assertEquals(1, spyPrinter.callCount);
        Assertions.assertEquals(purchase.content, spyPrinter.calledParams);
    }

    @Test
    void should_throw_hardwareException_when_process_given_printer_throw_out_of_paper_exception() {
        //given
        ExceptionPrinter spyPrinter = new ExceptionPrinter();
        CashRegister cashRegister = new CashRegister(spyPrinter);
        Purchase purchase = new Purchase();

        //when  //then
        Assertions.assertThrows(HardwareException.class, () -> cashRegister.process(purchase));
    }
}