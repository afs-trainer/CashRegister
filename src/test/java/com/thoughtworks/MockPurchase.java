package com.thoughtworks;

public class MockPurchase extends Purchase {

    public String content;

    @Override
    public String toString() {
        return content;
    }
}
