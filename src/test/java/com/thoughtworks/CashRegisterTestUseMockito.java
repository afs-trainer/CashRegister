package com.thoughtworks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;

class CashRegisterTestUseMockito {

    @Test
    void should_call_printer_when_process_given_purchase() {
        //given
        Printer spyPrinter = Mockito.spy(Printer.class);
        CashRegister cashRegister = new CashRegister(spyPrinter);
        Purchase purchase = new Purchase();

        //when
        cashRegister.process(purchase);

        //then
        Mockito.verify(spyPrinter, Mockito.times(1)).print(any());
    }

    @Test
    void should_call_printer_use_content_from_purchase_when_process_given_mock_purchase() {
        //given
        Printer spyPrinter = Mockito.spy(Printer.class);
        CashRegister cashRegister = new CashRegister(spyPrinter);
        Purchase purchase = Mockito.mock(Purchase.class);
        String givenContent = "given content";
        Mockito.when(purchase.toString()).thenReturn(givenContent);

        //when
        cashRegister.process(purchase);

        //then
        ArgumentCaptor<String> printParams = ArgumentCaptor.forClass(String.class);
        Mockito.verify(spyPrinter).print(printParams.capture());
        Assertions.assertEquals(givenContent, printParams.getValue());
    }

    @Test
    void should_throw_hardwareException_when_process_given_printer_throw_out_of_paper_exception() {
        //given
        Printer mockPrinter = Mockito.mock(Printer.class);
        CashRegister cashRegister = new CashRegister(mockPrinter);
        Purchase purchase = new Purchase();
        Mockito.doThrow(PrinterOutOfPaperException.class).when(mockPrinter).print(any());
        //when  //then
        Assertions.assertThrows(HardwareException.class, () -> cashRegister.process(purchase));
    }
}