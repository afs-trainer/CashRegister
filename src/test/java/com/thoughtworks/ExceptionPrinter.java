package com.thoughtworks;

public class ExceptionPrinter extends Printer {

    @Override
    public void print(String content) {
        throw new PrinterOutOfPaperException();
    }
}
