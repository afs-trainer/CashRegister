package com.thoughtworks;

public class SpyPrinter extends Printer {

    public int callCount = 0;
    public String calledParams;

    @Override
    public void print(String content) {
        calledParams = content;
        callCount++;
    }
}
