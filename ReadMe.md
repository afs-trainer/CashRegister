- ## Question 1

  We hope that CashRegister.process() can use Printer.print() to print Purchase. When the process(...) method is called, the Print() method should also be called.
- ## Question 2

  However, we don't want to use a real Printer for testing!
- ## Question 3

  At the same time, we'd like to make sure that when print() is triggered by process(), the printed content is from the processed purchase.
- ## Question 4

  If the printer is out of paper, process() should throw the specified exception.
- ## Question 5

  Use Mockito to eliminate all introduced classes just for tests.
- # Practice Requirement

    * Write the test code for the requirements as stated above ONE BY ONE.
    * You can ONLY modify test code, except Question
    * Don't use any 3rd-party Mock library until except Question
    * Please git commit once after each question is completed (commit message example: `test(question1): add test for CashRegister.Process`)